
Sesame

The module allows you to set up access to the site on base of request referer, remote IP address or User Agent with check of domain name.
The module creates different users for each referer/IP/User Agent. You can assign roles to these users and manage they permissions on the site.
This module can be used to set up access for search bots, web services like browsercam.com and so on.  

To install, place the entire module folder into your modules directory.

Go to Administer -> Site Building -> Modules and enable the Sesame module.

To set up access go to 
Administer -> Site Building -> IP/referer/user agent based access

For example, to give access to google bots:
1. Go to New agent tab
2. Fill fields:
- User Agent: Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)
- Domain name: /googlebot.com$/i
- Username: google_bot
3. Select roles for this user
4. Save